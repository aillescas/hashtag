### Visualization for hashcode challenge 2017 solutions ###

* This project is aimed to visualize the progression of different solutions for the Hash Code 2017 qualification round https://hashcode.withgoogle.com/past_editions.html

This is based on the team work from:

- Christian Expósito
- Javier Vázquez
- Jesus Gutierrez
- Antonio Illescas

### SETUP ###

- Install docker-engine https://docs.docker.com/engine/installation/
- Install docker-compose https://docs.docker.com/compose/install/
- run docker-compose up
- Wait untill server and client are up.
- In a browser connect to http://localhost:8888