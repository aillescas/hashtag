function getRandomInt(min, max){
         min = Math.ceil(min);
         max = Math.floor(max);
         return Math.floor(Math.random() * (max - min)) + min;
};

function listen(){
  var source = new WebSocket('ws://' + window.location.host + '/ws');
  var colors = ['blue', 'red', 'yellow', 'green', 'orange'];
  var svg = d3.select('svg');  
  source.onmessage = function(msg) {
    console.log(msg);
    var message = JSON.parse(msg.data);
    var x = message[0];
    var y = message[1];
    var color = getRandomInt(0, 5);
    svg.append('rect').attr('x', x).attr('y', y).attr('width', 3).attr('height', 3).attr('fill', colors[color]);
  }
}

$(document).ready(function(){
  listen();
});
