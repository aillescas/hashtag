import os
import tornado.gen as tgen
import tornado.ioloop
import tornado.queues as tq
import tornado.web as tw
import tornado.websocket as tws
import zmq
from zmq.eventloop.zmqstream import ZMQStream
from zmq.eventloop.ioloop import IOLoop


class WSocketHandler(tws.WebSocketHandler):
    CHANNELS = set()
    DATA_QUEUE = tq.Queue()

    def check_origin(self, origin):
        return True

    def open(self):
        print("New channel open")
        self.stream.set_nodelay(True)
        self.CHANNELS.add(self)

    def on_close(self):
        if self in self.CHANNELS:
            self.CHANNELS.remove(self)

    @classmethod
    @tgen.coroutine
    def publish(cls):
        while True:
            print("waiting for new data")
            item = yield cls.DATA_QUEUE.get()
            try:
                print("Processing %s" % item)
                for channel in cls.CHANNELS:
                    yield channel.write_message(item)
            except Exception as e:
                print(e)


class LocalBroker(object):
    TOPIC = "1"

    def __init__(self):
        self.context = zmq.Context()
        self.data_queue = self.context.socket(zmq.SUB)
        self.data_queue.connect(os.environ.get('ZMQ_ADDRESS', 'tcp://127.0.0.1:8889'))
        self.data_queue.setsockopt_string(zmq.SUBSCRIBE, self.TOPIC)
        self.stream = ZMQStream(self.data_queue)
        print("Conectando a {}".format(os.environ.get("ZMQ_ADDRESS")))
        self.stream.on_recv(self.receive_data)

    def receive_data(self, data):
        print("Received data {}".format(data))
        for message in data:
            message_content = message.split()
            WSocketHandler.DATA_QUEUE.put(b' '.join(message_content[1:]).decode('utf-8'))


if __name__ == "__main__":
    print("Init Server")
    current_dir = os.path.dirname(os.path.abspath(__file__))
    static_folder = os.path.join(current_dir, 'static')
    main_template = os.path.join(current_dir, 'templates', 'main.html')
    local_broker = LocalBroker()
    print("Conneted to socket ")
    app = tw.Application([
        (r'/ws', WSocketHandler),
        (r'/()', tornado.web.StaticFileHandler, {'path': main_template}),
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_folder})
    ])
    app.listen(8888)

    loop = IOLoop.current()
    loop.add_callback(WSocketHandler.publish)
    loop.start()
