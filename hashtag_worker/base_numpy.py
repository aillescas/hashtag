import datetime
import json
import numpy as np
import sys

import zmq


class DataPublisher(object):
    socket = None
    context = None

    @classmethod
    def initialize(cls):
        if not cls.context:
            cls.context = zmq.Context.instance()
        cls.socket = cls.context.socket(zmq.PAIR)
        cls.socket.bind("ipc://data.ipc")

    @classmethod
    def get_socket(cls):
        if not cls.socket:
            cls.initialize()
        return cls.socket

    @classmethod
    def sync(cls, sync_connection):
        if not cls.context:
            cls.context = zmq.Context()
        print("Sync with connection {}".format(sync_connection))
        cls.sync_socket = cls.context.socket(zmq.PAIR)
        cls.sync_socket.connect(sync_connection)
        # We just block reading the sync connection socket, the info is not important
        cls.sync_socket.recv()
        cls.sync_socket.close()


def read_dataset(filename):
    with open(filename) as data:
        # V number of videos
        # E number of endpoints
        # R number of requests
        # C number of cache servers
        # X capacyty of server
        V, E, R, C, X = map(int, data.readline().strip().split())

        videos = list(map(int, data.readline().strip().split()))

        datos_cache = np.array([0] * (C * E), dtype=np.float32).reshape(E, C)
        datos_videos = np.array([0] * (V * E), dtype=np.float32).reshape(V, E)

        endpoints = []
        for element in range(E):
            L, K = map(int, data.readline().strip().split())
            endpoint = {'latency': L, 'caches': [], 'videos_req': [], 'caches_id': [], 'videos_id': []}
            for connection in range(K):
                c, Lc = map(int, data.readline().strip().split())
                endpoint['caches'].append({'latency': Lc, 'id': c})
                endpoint['caches_id'].append(c)
                datos_cache[element][c] = L - Lc
            endpoints.append(endpoint)

        requests = []
        for request in range(R):
            Rv, Re, Rn = map(int, data.readline().strip().split())
            request_dict = {'video_id': Rv, 'endpoint_id': Re, 'n_requests': Rn}
            requests.append(request_dict)
            endpoints[Re]['videos_req'].append(request_dict)
            endpoints[Re]['videos_id'].append(Rv)
            weight = (X - videos[Rv]) / float(X)
            datos_videos[Rv][Re] = Rn * weight
        caches = [X] * C
        return caches, videos, datos_videos, datos_cache


def score_matrix(video_matrix, cache_matrix):
    return np.einsum('ij, jk->ik', video_matrix, cache_matrix)


def matrix_solution(caches, video_size, video_matrix, cache_matrix):
    solution = [[_] for _ in range(len(caches))]
    score = score_matrix(video_matrix, cache_matrix)
    # at most we will do caches * video solutions
    socket = DataPublisher.get_socket()
    for i in range(len(caches) * len(video_size)):
        video, cache = np.unravel_index(score.argmax(), score.shape)
        if score[video][cache] <= 0:
            break
        if caches[cache] >= video_size[video]:
            caches[cache] -= video_size[video]
            solution[cache].append(video)
            msg = json.dumps([str(cache), str(video)])
            print("Sending message {}".format(msg))
            socket.send(bytes(msg.encode('utf-8')))
            # Remove the video from the endpoints where is served by cache
            endpoints_served = np.copy(cache_matrix[:, cache])
            # We are going to convert this in a mask of 0 for every value > 0 and 1 for the 0's
            endpoints_served[endpoints_served == 1] = 2  # just in case
            endpoints_served[endpoints_served == 0] = 1
            endpoints_served[endpoints_served > 1] = 0
            video_matrix[video] *= endpoints_served
            # now we need to recalculate the scores
            score = score_matrix(video_matrix, cache_matrix)
        else:
            # The video does not fit in the cache so we have to invalidate this score
            score[video][cache] = 0
    return [' '.join(map(str, s)) for s in solution if len(s) > 1]


def solution(name, sync_connection):
    # Use sync_connection to find if the receiver is ready
    DataPublisher.sync(sync_connection)
    # The receiver is ready, connect the outgoing socket
    DataPublisher.initialize()

    caches, video_sizes, video_matrix, cache_matrix = read_dataset(name + '.in')

    matrix_solution(caches, video_sizes, video_matrix, cache_matrix)


if __name__ == '__main__':
    name = sys.argv[1]
    print("Start time for {}: {}".format(name, datetime.datetime.now()))
    caches, video_sizes, video_matrix, cache_matrix = read_dataset(name + '.in')

    result = matrix_solution(caches, video_sizes, video_matrix, cache_matrix)
    filename = name + datetime.datetime.now().strftime('%Y_%m_%d') + '_numpy.out'
    with open(filename, 'w') as output:
        output.write('{}\n'.format(len(result)))
        for s in result:
            output.write(s + '\n')
    print("End time for {}: {}".format(name, datetime.datetime.now()))
