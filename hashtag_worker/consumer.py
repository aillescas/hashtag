import random
import zmq

port = '8889'

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://127.0.0.1:%s" % port)

topic = random.randrange(9999, 10005)
socket.setsockopt_string(zmq.SUBSCRIBE, str(topic))
print("Suscribed to {}".format(topic))
while True:
    message = socket.recv_string()
    topic, message_data = message.split()
    print('Message received: {}'.format(message))
print('End')
