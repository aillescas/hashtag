import datetime
import sys

from itertools import chain


def read_dataset(filename):
    with open(filename) as data:
        # V number of videos
        # E number of endpoints
        # R number of requests
        # C number of cache servers
        # X capacyty of server
        V, E, R, C, X = map(int, data.readline().strip().split())

        videos = list(map(int, data.readline().strip().split()))

        endpoints = []
        for element in range(E):
            L, K = map(int, data.readline().strip().split())
            endpoint = {'latency': L, 'caches': [], 'videos_req': [], 'caches_id': [], 'videos_id': []}
            for connection in range(K):
                c, Lc = map(int, data.readline().strip().split())
                endpoint['caches'].append({'latency': Lc, 'id': c})
                endpoint['caches_id'].append(c)
            endpoints.append(endpoint)

        requests = []
        for request in range(R):
            Rv, Re, Rn = map(int, data.readline().strip().split())
            request_dict = {'video_id': Rv, 'endpoint_id': Re, 'n_requests': Rn}
            requests.append(request_dict)
            endpoints[Re]['videos_req'].append(request_dict)
            endpoints[Re]['videos_id'].append(Rv)

        return {'initial': (V, E, R, C, X), 'videos': videos, 'endpoints': endpoints, 'requests': requests}


def calculate_video_cache_score(data):
    endpoints = data['endpoints']
    cache_size = float(data['initial'][-1])
    for endpoint in endpoints:
        video_cache_reduction = {}
        for v in endpoint['videos_req']:
            for c in endpoint['caches']:
                key = str(c['id']) + '-' + str(v['video_id'])
                value = (v['n_requests'] * (endpoint['latency'] - c['latency']))
                weight = (cache_size - data['videos'][v['video_id']]) / cache_size
                video_cache_reduction[key] = value * weight
        endpoint['reduction'] = video_cache_reduction

    return data


def totalize_scores(data):
    total = {}
    for endpoint in data['endpoints']:
        for k, v in endpoint.get('reduction', {}).iteritems():
            score = total.get(k, 0)
            score += v
            total[k] = score
    data['total_score_dict'] = total
    data['total_score'] = [(k, v) for k, v in total.iteritems()]
    data['total_score'].sort(key=lambda x: x[1], reverse=True)
    return data


def recalculate_scores(data, cached_video, cache_used):
    """This works a little bit better than already_cached optimization for videos_worth_spreading.
    """
    loss = {}
    for endpoint in data['endpoints']:
        if cache_used in endpoint['caches_id'] and cached_video in endpoint['videos_id']:
            # For all the caches in this endpoint this video is going to reduce its weight in the
            # total_score
            for id in (str(c) + '-' + str(cached_video) for c in endpoint['caches_id']):
                lossed_score = loss.get(id, 0)
                lossed_score += endpoint['reduction'][id]
                loss[id] = lossed_score
    # apply lossed_score if necessary
    for key, loss_value in loss.iteritems():
        data['total_score_dict'][key] -= loss_value
    data['total_score'] = None
    data['total_score'] = [(k, v) for k, v in data['total_score_dict'].iteritems()]
    data['total_score'].sort(key=lambda x: x[1], reverse=True)
    loss = None
    return data


def solution(data):
    data = calculate_video_cache_score(data)
    data = totalize_scores(data)

    data['cache_size'] = [data['initial'][4] for _ in range(data['initial'][3])]
    caches = data['cache_size']
    video_length = data['videos']

    solution = {}
    # Videos worth spreading works better without already_cached optimization
    # already_cached = []
    # at most we will do caches * video solutions
    for i in range(len(caches) * len(video_length)):
        # The candidate to be cached is going to be allways the first item here.
        analysing = data['total_score'][0]
        if analysing[1] <= 0:
            # There is no more improvement in storing videos in cache, we have finished.
            break
        cache, video = map(int, analysing[0].split('-'))
        if caches[cache] >= video_length[video]:
            videos_in_cache = solution.get(cache, [])
            videos_in_cache.append(video)
            caches[cache] -= video_length[video]
            solution[cache] = videos_in_cache
            # This is is a extremely expensive operation, like two orders of magnitude
            # more expensive. (From 15 min to 36h <2160 min>)
            data = recalculate_scores(data, video, cache)
        else:
            # Just forget the first element as it is not cacheable.
            data['total_score'] = data['total_score'][1:]
    # for score in data['total_score']:
    #     cache, video = map(int, score[0].split('-'))
    #     if caches[cache] >= video_length[video] and video not in already_cached:
    #         videos_in_cache = solution.get(cache, [])
    #         videos_in_cache.append(video)
    #         caches[cache] -= video_length[video]
    #         solution[cache] = videos_in_cache
    #         already_cached.append(video)
    return [' '.join(map(str, chain([k], v))) for k, v in solution.iteritems()]


if __name__ == '__main__':
    name = sys.argv[1]
    print("Start time for {}: {}".format(name, datetime.datetime.now()))
    data = read_dataset(name + '.in')
    result = solution(data)
    filename = name + datetime.datetime.now().strftime('%Y_%M_%d') + '.out'
    with open(filename, 'w') as output:
        output.write('{}\n'.format(len(result)))
        for s in result:
            output.write(s + '\n')
    print("End time for {}: {}".format(name, datetime.datetime.now()))
