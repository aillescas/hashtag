import os

import zmq

from multiprocessing import Process

from base_numpy import solution

port = '8889'
print("Init worker")
context = zmq.Context.instance()
socket = context.socket(zmq.PUB)
environ_addrs = os.environ.get("ZMQ_ADDRESS")
print("Conectando a {}".format(environ_addrs))
socket.bind(environ_addrs or "tcp://*:%s" % port)
print("Conneted to socket {}".format(environ_addrs))
topic = 1

incoming_socket = context.socket(zmq.PAIR)
incoming_socket.connect("ipc://data.ipc")
file_name = 'me_at_the_zoo'
sync_connection = "ipc://sync.ipc"
topic = 1

Process(target=solution, args=(file_name, sync_connection,)).start()
sync_socket = context.socket(zmq.PAIR)
sync_socket.bind(sync_connection)
sync_socket.send(b'sync')
while True:
    print("Waiting for data")
    messagedata = incoming_socket.recv()
    print("Message received {}".format(messagedata))
    socket.send_string("{} {}".format(topic, messagedata.decode('utf-8')))
    print("Message sent {}".format(messagedata))
